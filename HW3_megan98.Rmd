---
title: "HW3_megan98"
author: "Megan Rachfalski"
date: "2/7/2019"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## Problem 1

Work through the "Getting_and_Cleaning_Data" *swirl* lesson part 3 and "Exploratory_Data_Analysis" *swirl* lesson parts 2, 5 and 7. 

From the R command prompt:  
install.packages("swirl")  
library(swirl)  
install_course("Getting_and_Cleaning_Data")  
swirl()  

_Nothing to turn in_

## Problem 2

Read through the Git help Chapters 1 and 2. <https://git-scm.com/book/en/v2>

Just good stuff to know, you will NOT be tested on it but may find it helpful if you want a little more Git.

_Nothing to turn in_

## Problem 3

Create a new R Markdown file (file-->new-->R Markdown; author = you, choose PDF).

The filename should be: HW3_pid, i.e. for me it would be HW3_rsettlag.

You will use this new R Markdown file to solve problems 4-7.
  
## Problem 4 [1 point]

Scenario: You are given a dataset and being a good data scientist, import, munge and summarize the data.  The summary stats are given in Table 1:  

```{r echo=F, eval=T, include=T}
require(stats); require(graphics)
data_summary <- data.frame(apply(anscombe,MARGIN = 2, 
                    function(x) c(mean(x),sd(x))),
                    row.names=c("mean","sd"))
knitr::kable(round(data_summary,2),caption = "summary of Anscombe dataset")
```

## Part A.
What are your initial thoughts? 

**My initial thoughts are that the means and standard deviations are the same for all x's and are the same for all the y's.**

You proceed with creating a linear model as per your collaborators requirements and get the following coefficients:  

```{r echo=F, eval=T, include=T}
ff <- y ~ x
mods <- setNames(as.list(1:4), paste0("lm", 1:4))
for(i in 1:4) {
  ff[2:3] <- lapply(paste0(c("y","x"), i), as.name)
  ## or   ff[[2]] <- as.name(paste0("y", i))
  ##      ff[[3]] <- as.name(paste0("x", i))
  mods[[i]] <- lmi <- lm(ff, data = anscombe)
}
knitr::kable(sapply(mods, coef),caption="linear model for x1 vs y1, x2 vs y2, etc in Anscombe data")
```

## Part B.
What are your thoughts after seeing the regression coefficient output? 

**All the linear models representing x1 are extremely close, almost identical, in the values that they produce.**

## Part C.
You then plot the data to obtain the result shown in Figure 1 below.  What is the lesson here?

```{r echo=F, eval=T, include=T, fig.height=4}
## Now, do what you should have done in the first place: PLOTS
op <- par(mfrow = c(2, 2), mar = 0.1+c(4,4,1,1), oma =  c(0, 0, 2, 0))
for(i in 1:4) {
  ff[2:3] <- lapply(paste0(c("y","x"), i), as.name)
  plot(ff, data = anscombe, col = "red", pch = 21, bg = "orange", cex = 1.2,
       xlim = c(3, 19), ylim = c(3, 13))
  abline(mods[[i]], col = "blue")
}
mtext("Linear model for Anscombe's data", outer = TRUE, cex = 1.5)
par(op)
```

**The lesson here is that varying distributions can have the same summary statistics and linear models. Thus, from this we learn the importance of plotting so you can visualize the data more thoroughly and accurately.**

## Problem 5

In these exercises, you will import, munge, clean and summarize datasets from Wu and Hamada's _Experiments: Planning, Design and Analysis_ book used in the graduate experiment design class.  For each dataset, please weave your code and text to describe both your process and observations.  Make sure you create a tidy dataset describing the variables, create a summary table of the data, note any issues with the data, and create a single plot of each dataset highlighting some feature of the data.  For full credit, you will use the dplyr and tidyr packages for munging and summarizing the data. Remember to label your plots including axis and any necessary legends.   

## Part A  [3 points]  
Ten parts were selected randomly from a line and duplicate measurements of the part's wall thickness were taken by each of three operators of the measurement apparatus.    
<https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/ThicknessGauge.dat>  

```{r gauge_data, echo=T, eval=T}
#Establishes library necesssary to perform function
library(dplyr)

#Reads in the required dataset
url<-"https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/ThicknessGauge.dat"

thicknessgauge_raw<-read.table(url, header=F, skip=2, fill=T, stringsAsFactors = F)

#Tidys the dataset  
thicknessgauge_tidy<-thicknessgauge_raw %>%  
    mutate(Part=V1, Thickness1=(V2+V3)/2, Thickness2=(V4+V5)/2, Thickness3=(V6+V7)/2) %>%
    select(Part,Thickness1, Thickness2, Thickness3)
thicknessgauge_tidy
```

**The first operator, represented by Thickness 1, on average appears to be the lowest. The third operator, represented by Thickness 3, appears to be the highest on average **

```{r gauge_summary, echo=T, include=T, eval=T, results='asis'}
    #Displays the datasets table summary
    knitr::kable(summary(thicknessgauge_tidy), caption="Thickness Gauge Operator Summary")
```

**The summary statistics confirm that the mean of operator 1, respresented by Thickness 1, is lowest and the mean of operator 3, represented by Thickness 3, is the highest.**

```{r gauge_plot, echo=T, include=T, eval=T}
#Sets up the boxplot data
thicknessgauge_tidy1 <- thicknessgauge_tidy[,c(1, 2)]
colnames(thicknessgauge_tidy1) <- c("Part", "Thickness")
thicknessgauge_tidy1$operator <- "Operator 1"

thicknessgauge_tidy2 <- thicknessgauge_tidy[,c(1, 3)]
colnames(thicknessgauge_tidy2) <- c("Part", "Thickness")
thicknessgauge_tidy2$operator <- "Operator 2"

thicknessgauge_tidy3 <- thicknessgauge_tidy[,c(1, 4)]
colnames(thicknessgauge_tidy3) <- c("Part", "Thickness")
thicknessgauge_tidy3$operator <- "Operator 3"

#Combines the data into one dataset
thicknessgauge_plot <- rbind(thicknessgauge_tidy1, thicknessgauge_tidy2, 
                             thicknessgauge_tidy3)

#Creates boxplot that represents the data
boxplot(thicknessgauge_plot$Thickness~thicknessgauge_plot$operator, 
        main = "Thickness Gauge Operator Plots")

```

**You can see represented in the mean line of the boxplot that it is further proven that operator 1 has the lowest mean while operator 3 has the highest.**

## Part B [3 points]
Larvae counts at two ages given 5 different treatments in 8 blocks.  
<https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/LarvaeControl.dat>  

```{r larvae_data, echo=T, eval=T}
#Establishes the required library to perform functions
library(dplyr)

#Reads in the data
url<-"https://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/LarvaeControl.dat"

larvaelength_raw<-read.table(url, header=F, skip=3, fill=T, stringsAsFactors = F)

#Tidys the dataset
larvaelength_tidy<-larvaelength_raw %>%  
    mutate(Larvae=V1, Length1=(V2+V7)/2, Length2=(V3+V8)/2, 
           Length3=(V4+V9)/2, Length4=(V5+V10)/2, Length5=(V6+V11)/2) %>%
    select(Larvae, Length1, Length2, Length3, Length4, Length5)
larvaelength_tidy
```

**Through analyzing the dataset it is difficult to establish any conclusions on observations considering that the Lengths vary widely within each block and replication. Thus, in order to predict anything about the dataset I will use the summary statistics established below**

```{r larvae_summary, echo=T, include=T, eval=T, results='asis'}
    #Shows the datasets table summary
    
knitr::kable(summary(larvaelength_tidy), caption="Treatment Effect On Larvae Length")
```

**The summary statistics allow me to conclude that treatmeant 5, represented by Length 5, has the lowest mean length while treatment 1, represented by Length 1, has the highest mean length.**

```{r larvae_plot, echo=T, include=T, eval=T}
#Establishes the data that is going to be used to set up plots
larvaelength_tidy1 <- larvaelength_tidy[,c(1, 2)]
colnames(larvaelength_tidy1) <- c("Larvae", "Length")
larvaelength_tidy1$treatment <- "Treatment 1"

larvaelength_tidy2 <- larvaelength_tidy[,c(1, 3)]
colnames(larvaelength_tidy2) <- c("Larvae", "Length")
larvaelength_tidy2$treatment <- "Treatment 2"

larvaelength_tidy3 <- larvaelength_tidy[,c(1, 4)]
colnames(larvaelength_tidy3) <- c("Larvae", "Length")
larvaelength_tidy3$treatment <- "Treatment 3"

larvaelength_tidy4 <- larvaelength_tidy[,c(1, 5)]
colnames(larvaelength_tidy4) <- c("Larvae", "Length")
larvaelength_tidy4$treatment <- "Treatment 4"

larvaelength_tidy5 <- larvaelength_tidy[,c(1, 6)]
colnames(larvaelength_tidy5) <- c("Larvae", "Length")
larvaelength_tidy5$treatment <- "Treatment 5"

#Binds all the data together in order to input it into the boxplot function
larvaelength_plot <- rbind(larvaelength_tidy1, larvaelength_tidy2, 
                           larvaelength_tidy3, larvaelength_tidy4, 
                           larvaelength_tidy5)

#Creates boxplot that represents the dataset
boxplot(larvaelength_plot$Length~larvaelength_plot$treatment, 
        main = "Treatment Effect on Larvae Length Plot")
```

**The boxplot allows me to further prove that treatment 5 has the lowest mean which you can see represented by the mean line within the plot. It also proves that treatment 1 also represents the highest mean through the mean line in the plots. We can also conclude that they have relatively similar means though different enough to form conclusions on which had a greater or lesser effect than the others.**

# Problem 6 [1 point]
Please knit this document to PDF (name should be `HW3_pid`) and push to BitBucket:

In the R Terminal, type:  
\begin{enumerate}
    \item git pull  
    \item git add HW3\_pid.[pR]*  (NOTE: this should add two files)  
    \item git commit -m "final HW3 submission"   
    \item git push
\end{enumerate}

# Optional Extra Credit [1 point]
Tomato yield data.  Two tomato varieties were planted at 3 different densities.  The experiment was run in triplicate.  I have created a tidy dataset using tidyr and dplyr.  You job is to duplicate this using base R functions.  

```{r Problem5_Tomato_analysis, echo=T, eval=T, include=F}
    ########################### 
    #Problem5_Tomato_analysis  
    #get data  
    ########################### 
    library(tidyverse)
    url<-"http://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/tomato.dat"
    Tomato_raw<-read.table(url, header=F, skip=2, fill=T, stringsAsFactors = F, comment.char = "")
    Tomato_tidy<-Tomato_raw %>%  
        separate(V2,into=paste("C10000",1:3,sep="_"),sep=",",remove=T, extra="merge") %>%
        separate(V3,into=paste("C20000",1:3,sep="_"),sep=",",remove=T, extra="merge") %>%
        separate(V4,into=paste("C30000",1:3,sep="_"),sep=",",remove=T, extra="merge") %>%
        mutate(C10000_3=gsub(",","",C10000_3)) %>%
        gather(Clone,value,C10000_1:C30000_3) %>%
        mutate(Variety=V1, Clone=gsub("C","",Clone)) %>%
        mutate(Variety=gsub("\\\\#"," ",Variety)) %>%
        separate(Clone,into = c("Clone","Replicate")) %>%
        select(-V1,Variety,Clone,value) %>%
        arrange(Variety) 
    Tomato_tidy
        
    ########################### 
```

The finished product is given in Table 1 below.

```{r Tomato_table, echo=T, include=T, eval=T, results='asis'}
    knitr::kable(summary(Tomato_tidy), caption="Tomato data summary")
```

```{r Tomato_data, echo=T, include=T, eval=T}
    url<-"http://www2.isye.gatech.edu/~jeffwu/wuhamadabook/data/tomato.dat"
    Tomato_raw<-read.table(url, header=F, skip=2, fill=T, stringsAsFactors = F, 
                           comment.char = "")
    
    #Creating the values list for Ife 1
    ife_val <- append(append(unlist(strsplit(Tomato_raw$V2[1], ",")), 
                             unlist(strsplit(Tomato_raw$V3[1], ","))), 
                      unlist(strsplit(Tomato_raw$V4[1], ",")))
    
    #Creating the values list for PusaEarlyDwarf
    pusa_val <- append(append(unlist(strsplit(Tomato_raw$V2[2], ",")), 
                              unlist(strsplit(Tomato_raw$V3[2], ","))), 
                       unlist(strsplit(Tomato_raw$V4[2], ",")))
    
    #Parsing the raw data into lists to form the final dataset
    values_list <- append(ife_val, pusa_val)
    replicates_list <- rep(c("1", "2", "3"), 6)
    variety_list <- append(rep(gsub('\\\\#', ' ', Tomato_raw$V1[1]), 9), 
                           rep(Tomato_raw$V1[2],9))
    clone_list <- rep(append(append(rep('10000',3), rep('20000',3)), 
                             rep('30000',3)),2)
    
    #Combining the lists created from raw data
    Tomato_tidy <- data.frame(as.matrix(clone_list), stringsAsFactors=FALSE)
    colnames(Tomato_tidy) <- c('Clone')
    Tomato_tidy$Replicate <- replicates_list
    Tomato_tidy$value <- values_list
    Tomato_tidy$Variety <- variety_list
    
    Tomato_tidy
```

```{r Tomato_summary, echo=T, include=T, eval=T, results='asis'}
    #Provides the table summary of created dataset
    knitr::kable(summary(Tomato_tidy), caption="Tomato data summary")
```



## Grading Rubric:

\begin{itemize}
    \item 1 point: successfully submitted to BitBucket  
    \item 2 points: Neat, well written document  
    \item 1 point: reasonable response to question 4  
    \item 6 points: correct answers to problem 5 as given  
    \item 1 bonus point  
\end{itemize}